from __future__ import absolute_import

import sys
import errno
import subprocess
import socket

import pytest

from simpy._compat import PY2


def test_pipe(env, pipe_type):
    readpipe, writepipe = pipe_type.pair(env)

    yield writepipe.write(b'foo')
    result = yield readpipe.read(1024)
    assert result == b'foo'


def test_pipe_nonblocking(env, pipe_type):
    readpipe, writepipe = pipe_type.pair(env)

    read_ev = readpipe.read(1024)
    yield writepipe.write(b'foo')
    result = yield read_ev
    assert result == b'foo'


def test_read_close(env, pipe_type):
    readpipe, writepipe = pipe_type.pair(env)

    yield writepipe.write(b'foo') & readpipe.read(1024)

    readpipe.close()

    try:
        yield writepipe.write(b'foo')
        assert False, 'Expected an exception'
    except OSError as e:
        assert e.errno == errno.EPIPE


def test_write_close(env, pipe_type):
    readpipe, writepipe = pipe_type.pair(env)

    yield writepipe.write(b'foo') & readpipe.read(1024)

    writepipe.close()

    try:
        yield readpipe.read(1024)
        assert False, 'Expected an exception'
    except OSError as e:
        assert e.errno == errno.EPIPE
    except socket.error as e:
        assert PY2 and e.errno == errno.EPIPE


def test_subprocess(env, pipe_type):
    child = subprocess.Popen([sys.executable, '-c',
            ('from __future__ import print_function; import sys;'
                'print(\'echo: %s\' % sys.stdin.readline()[:-1])')],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE)

    readpipe = pipe_type(env, child.stdout.fileno())
    writepipe = pipe_type(env, child.stdin.fileno())

    yield writepipe.write(b'spam\n')
    result = yield readpipe.read(1024)
    assert result == b'echo: spam\n'

    try:
        yield readpipe.read(1024)
        assert False, 'Expected an exception'
    except OSError as e:
        assert e.errno == errno.EPIPE
    except socket.error as e:
        assert PY2 and e.errno == errno.EPIPE

    child.wait()
